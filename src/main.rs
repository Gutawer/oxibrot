use num::Complex;
use rgb::*;
use rayon::prelude::*;
use pixels::{wgpu::Surface, Pixels, SurfaceTexture};
use winit::{
	event_loop::{ControlFlow, EventLoop},
	window::WindowBuilder,
	event::{
		Event, WindowEvent, KeyboardInput,
		MouseScrollDelta, MouseButton, ElementState, VirtualKeyCode
	},
	dpi::PhysicalSize
};
use ndarray::{s, Array2};
use std::{
	sync::{
		Arc,
		Mutex,
		Condvar
	},
	path::Path,
	fs::File,
	io::{BufWriter, Write}
};
use palette::{gradient::Gradient, rgb::LinSrgb};
use log::*;

mod perturbation;

#[derive(Debug, Copy, Clone)]
enum MandelResult {
	Glitched,
	Bounded,
	Unbounded(f64)
}

#[derive(Debug)]
struct ViewState {
	// centre of the screen
	centre_x: rug::Float,
	centre_y: rug::Float,
	size: f64,

	clicking: bool,

	mouse_x: i32,
	mouse_y: i32,

	recompute: bool,

	// to save on allocations, the pixel buffer is always the same size,
	// therefore we just track the resolution and take slices (thanks ndarray!)
	cur_res: (usize, usize),
	pixels: Array2<RGB8>
}

const SIZE_X: usize = 1200;
const SIZE_Y: usize =  800;
const BITS: u32     = 1024;

fn calculate_bounds(centre: &rug::Complex, size: f64, ratio: f64) -> (rug::Complex, rug::Complex) {
	if ratio >= 1.0 {
		let mut bounds_tl = centre.clone();
		*bounds_tl.mut_imag() += size;
		*bounds_tl.mut_real() -= size * ratio;
		let mut bounds_br = centre.clone();
		*bounds_br.mut_imag() -= size;
		*bounds_br.mut_real() += size * ratio;
		(bounds_tl, bounds_br)
	} else {
		let mut bounds_tl = centre.clone();
		*bounds_tl.mut_real() -= size;
		*bounds_tl.mut_imag() += size / ratio;
		let mut bounds_br = centre.clone();
		*bounds_br.mut_real() += size;
		*bounds_br.mut_imag() -= size / ratio;
		(bounds_tl, bounds_br)
	}
}

impl ViewState {
	fn new() -> Self {
		Self {
			centre_x: rug::Float::with_val(BITS, -0.5f64),
			centre_y: rug::Float::with_val(BITS,    0f64),
			size: 2f64,

			clicking: false,

			mouse_x: 0,
			mouse_y: 0,

			recompute: true,

			cur_res: (1, 1),
			pixels: Array2::default((SIZE_Y, SIZE_X))
		}
	}

	fn handle_input(
		state: &Arc<(Mutex<ViewState>, Condvar)>, event: &Event<()>,
		size: (usize, usize)
	) -> bool {

		let (state, _) = &**state;
		let mut state = state.lock().expect("Expected to lock mutex");
		match event {
			Event::WindowEvent { event, .. } => {
				match event {
					WindowEvent::MouseWheel { delta: MouseScrollDelta::LineDelta(_, y), .. } => {
						let (aim_x, aim_y) = {
							let bounds = calculate_bounds(
								&rug::Complex::with_val(BITS, (state.centre_x.clone(), state.centre_y.clone())),
								state.size,
								SIZE_X as f64 / SIZE_Y as f64
							);
							(
								perturbation::remap_range(
									state.mouse_x as f64,
									(0f64, SIZE_X as f64),
									(bounds.0.real(), bounds.1.real())
								),
								perturbation::remap_range(
									state.mouse_y as f64,
									(0f64, SIZE_Y as f64),
									(bounds.0.imag(), bounds.1.imag())
								)
							)
						};
						if *y > 0.0 {
							state.size *= 0.5;
							// to keep the aimed at point at the same point on screen, take the average of it and the centre
							state.centre_x = 0.5 * (state.centre_x.clone() + aim_x);
							state.centre_y = 0.5 * (state.centre_y.clone() + aim_y);
							state.recompute = true;
						} else if *y < 0.0 {
							state.size *= 2.0;
							// this is the inverse of the zooming in equation
							state.centre_x = 2.0 * state.centre_x.clone() - aim_x;
							state.centre_y = 2.0 * state.centre_y.clone() - aim_y;
							state.recompute = true;
						}
						println!("New size: {:e}", state.size);
						true
					},
					WindowEvent::MouseInput { button: MouseButton::Left, state: button_state, .. } => {
						match button_state {
							ElementState::Pressed  => { state.clicking = true  },
							ElementState::Released => { state.clicking = false }
						}
						true
					},
					WindowEvent::KeyboardInput{ input: KeyboardInput {
						virtual_keycode: Some(VirtualKeyCode::R),
						state: ElementState::Pressed,
						..
					}, .. } => {
						state.size = 2.0;
						state.centre_x = rug::Float::with_val(BITS, -0.5f64);
						state.centre_y = rug::Float::with_val(BITS,    0f64);
						state.recompute = true;
						true
					}
					WindowEvent::KeyboardInput{ input: KeyboardInput {
						virtual_keycode: Some(VirtualKeyCode::P),
						state: ElementState::Pressed,
						..
					}, .. } => {

						let path = Path::new(r"mandel.png");
						match File::create(path) {
							Ok(file) => if let Err(e) = (|| -> Result<(), png::EncodingError> {
								let w = BufWriter::new(file);

								let mut encoder = png::Encoder::new(w, state.cur_res.0 as u32, state.cur_res.1 as u32);
								encoder.set_color(png::ColorType::RGB);
								encoder.set_depth(png::BitDepth::Eight);
								let writer = encoder.write_header()?;
								let mut stream_writer = writer.into_stream_writer_with_size(state.cur_res.0);

								for row in state.pixels.slice(s![..size.1, ..size.0]).outer_iter() {
									stream_writer.write(row.to_slice().unwrap().as_bytes())?;
								}

								stream_writer.finish()?;
								
								Ok(())
							})() {
								error!("{:?}", e);
							},
							Err(_) => { error!("Unable to create a file"); }
						}
						true
					},
					WindowEvent::CursorMoved { position, .. } => {
						let (x, y): (i32, i32) = (*position).into();
						if state.clicking {
							let delta_x = x - state.mouse_x;
							let delta_y = y - state.mouse_y;
							let x_change = 2.0 * delta_x as f64 * state.size / size.0 as f64;
							let y_change = 2.0 * delta_y as f64 * state.size / size.1 as f64;
							state.centre_x -= x_change;
							state.centre_y += y_change;
							state.recompute = true;
						}
						state.mouse_x = x as i32;
						state.mouse_y = y as i32;
						true
					},
					_ => false
				}
			},
			_ => false,
		}
	}
}

fn render(state_arc: Arc<(Mutex<ViewState>, Condvar)>, stop_channel: crossbeam::Receiver<()>) {
	let mut pixels = Array2::default((SIZE_Y, SIZE_X));
	let gradient = Gradient::new([
		LinSrgb::new(  0f64, 0f64, 0f64),
		LinSrgb::new(  0f64, 0f64, 1f64),
		LinSrgb::new(0.5f64, 0f64, 1f64),
		LinSrgb::new(  1f64, 1f64, 1f64),
		LinSrgb::new(  1f64, 1f64, 0f64),
		LinSrgb::new(  1f64, 0f64, 0f64),
		LinSrgb::new(  0f64, 0f64, 0f64),
	].iter().cloned());
	'main: loop {
		let (state, recompute) = &*state_arc;
		let state = state.lock().expect("Expected to lock mutex");
		let _ = recompute.wait(state).expect("Expected to wait on Condvar");

		let res = (SIZE_X, SIZE_Y);
		let mut iters = 1000;
		let mut reference = rug::Complex::with_val(BITS, 0f64);
		while iters <= 64000 {
			let _ = stop_channel.try_recv();
			let (cur_bounds, (reference_vec, glitch_vec)) = {
				let (state, _) = &*state_arc;
				let mut state = state.lock().expect("Expected to lock mutex");
				state.recompute = false;
				let cur_bounds = calculate_bounds(
					&rug::Complex::with_val(BITS, (state.centre_x.clone(), state.centre_y.clone())),
					state.size,
					SIZE_X as f64 / SIZE_Y as f64
				);
				let centre = rug::Complex::with_val(BITS, (state.centre_x.clone(), state.centre_y.clone()));
				if reference.real() < cur_bounds.0.real() || reference.real() > cur_bounds.1.real() ||
				   reference.imag() > cur_bounds.1.imag() || reference.imag() < cur_bounds.1.imag()    {
					reference =
						match perturbation::find_reference_point_in_view(
							(&cur_bounds.0, &cur_bounds.1), &centre, iters, 64, &stop_channel
						) {
							Ok(p) => p,
							Err(r) => match r {
								perturbation::ReferenceErrReason::NoneFound => if iters == 64000 {
									rug::Complex::with_val(BITS, 0f64)
								} else {
									iters *= 2;
									continue;
								},
								perturbation::ReferenceErrReason::Stopped   => continue 'main,
							}
						};
				}
				let pert_vecs = perturbation::generate_reference_vec(&reference, iters as usize);
				(cur_bounds, pert_vecs)
			};
			let cur_bounds = (&cur_bounds.0, &cur_bounds.1);

			let comp_result = perturbation::compute_chunk(
				&mut pixels, res, cur_bounds, &reference, &reference_vec, &glitch_vec, &gradient, &stop_channel
			);

			// comp_result contains whether the result was stopped by the user changing view
			if comp_result.is_none() { continue 'main; }

			let (state, _) = &*state_arc;
			let mut state = state.lock().expect("Expected to lock mutex");
			// this is like double buffering - we copy the buffer over
			state.pixels.assign(&pixels);
			state.cur_res = res;

			iters *= 2;

			if state.recompute { continue 'main }
		}
	}
}

fn main_canvas() {
	env_logger::init();

	let state = ViewState::new();
	let state = Mutex::new(state);
	let recompute = Condvar::new();

	let event_loop = EventLoop::new();
	let window = {
		let size = PhysicalSize::new(SIZE_X as f64, SIZE_Y as f64);
		WindowBuilder::new()
			.with_title("Mandelbrot")
			.with_inner_size(size)
			.with_min_inner_size(size)
			.build(&event_loop)
			.expect("Expected to create a window")
	};

	let mut pixels = {
		let surface = Surface::create(&window);
		let surface_texture = SurfaceTexture::new(SIZE_X as u32, SIZE_Y as u32, surface);
		Pixels::new(SIZE_X as u32, SIZE_Y as u32, surface_texture).expect("Expected to create pixels")
	};

	let state_arc = Arc::new((state, recompute));
	let (s_rayon, r_rayon) = crossbeam::channel::bounded(1);

	let state_arc_comp = state_arc.clone();
	std::thread::spawn(move || render(state_arc_comp, r_rayon));

	let mut state_arc_render = state_arc.clone();
	event_loop.run(move |event, _, control_flow| {
		match event {
			Event::RedrawRequested(_) => {
				let (state, recompute) = &*state_arc_render;
				let state = state.lock().expect("Expected to lock mutex");
				if state.recompute {
					// keep trying this until the renderer actually does something
					// this is pretty much necessary since this thread might run before
					// the render thread tries to wait, which causes a black screen upon initial launch
					recompute.notify_all();
					let _ = s_rayon.try_send(());
				}

				let image = pixels.get_frame();
				for (i, pixel) in image.chunks_exact_mut(4).enumerate() {
					let x = i % SIZE_X;
					let y = i / SIZE_X;

					let real_res = state.cur_res;
					let new_y = (y * real_res.0) / SIZE_X;
					let new_x = (x * real_res.1) / SIZE_Y;
					let p = state.pixels[(new_y, new_x)];
					
					pixel.copy_from_slice(&[
						p.r,
						p.g,
						p.b,
						255u8
					]);
				}

				if pixels.render().is_err() {
					*control_flow = ControlFlow::Exit;
					return;
				}
			},
			Event::WindowEvent { event: WindowEvent::CloseRequested, .. } => {
				*control_flow = ControlFlow::Exit;
				return;
			}
			_ => { ViewState::handle_input(&mut state_arc_render, &event, (SIZE_X, SIZE_Y)); }
		}

		window.request_redraw();
	});
}

fn main() {
	main_canvas()
}
