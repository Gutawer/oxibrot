use super::*;
use std::ops::*;
use simdeez::*;
use simdeez::scalar::*;
use simdeez::sse2::*;
use simdeez::sse41::*;
use simdeez::avx2::*;
use crossbeam::Receiver;
use std::sync::atomic::*;

#[derive(Debug, Clone)]
struct DualComplex {
	val: rug::Complex,
	diff: rug::Complex
}

impl DualComplex {
	fn variable(val: rug::Complex) -> DualComplex {
		let precision = val.prec();
		DualComplex {
			val,
			diff: rug::Complex::with_val(precision, (1.0, 0.0))
		}
	}

	fn constant(val: rug::Complex) -> DualComplex {
		let precision = val.prec();
		DualComplex {
			val,
			diff: rug::Complex::with_val(precision, (0.0, 0.0))
		}
	}

	fn complement(&self) -> DualComplex {
		DualComplex {
			val: self.val.clone(),
			diff: -self.diff.clone()
		}
	}
}

impl Add for &DualComplex {
	type Output = DualComplex;

	fn add(self, rhs: Self) -> Self::Output {
		let mut res = self.clone();
		res.val += &rhs.val;
		res.diff += &rhs.diff;
		res
	}
}

impl Add<&DualComplex> for DualComplex {
	type Output = DualComplex;

	fn add(self, rhs: &Self) -> Self::Output {
		&self + rhs
	}
}

impl Mul for &DualComplex {
	type Output = DualComplex;

	fn mul(self, rhs: Self) -> Self::Output {
		let mut res = self.clone();
		// (x + x'e)(y + y'e) = xy + (x'y + xy')e

		// xy
		res.val *= &rhs.val;

		// x'y
		res.diff *= &rhs.val;
		// x'y + xy'
		res.diff += &self.val * &rhs.diff;

		res
	}
}

impl Mul<&DualComplex> for DualComplex {
	type Output = DualComplex;

	fn mul(self, rhs: &Self) -> Self::Output {
		&self * rhs
	}
}

impl Div for &DualComplex {
	type Output = DualComplex;

	fn div(self, rhs: Self) -> Self::Output {
		let denom = rhs.val.clone().square();
		let numer = self * &rhs.complement();

		Self::Output {
			val: numer.val   / &denom,
			diff: numer.diff / &denom
		}
	}
}

impl Div<&DualComplex> for DualComplex {
	type Output = DualComplex;

	fn div(self, rhs: &Self) -> Self::Output {
		&self / rhs
	}
}

pub(super) fn generate_reference_vec(
	point: &rug::Complex, iteration_limit: usize
) -> (Vec<Complex<f64>>, Vec<f64>) {

	let glitch_tolerance = 1e-6;

	let mut reference = Vec::with_capacity(iteration_limit);
	let mut glitch_vec = Vec::with_capacity(iteration_limit);

	// a major part of the arbitrary precision implementation is this reference vec
	// ideally it should be as large as iteration_limit, but if the number becomes too large,
	// it's not worth tracking (i.e. it was a bad choice of point)
	let mut cur = point.clone();
	for _ in 0..iteration_limit {
		let (r, i) = cur.clone().into_real_imag();
		let new_val = Complex::new(r.to_f64(), i.to_f64());
		glitch_vec.push(new_val.norm_sqr() * glitch_tolerance);
		reference.push(new_val);

		if *cur.clone().norm().real() > 1000000.0 { continue; }

		cur.square_mut();
		cur += point;
	}

	(reference, glitch_vec)
}

fn signum(x: &rug::Float) -> i8 {
	if x > &0.0 { 1 } else if x < &0.0 { -1 } else { 0 }
}

enum LineHitResult {
	Sign(i8),
	DefinitelyIn
}

fn line_hits_positive_real(p0: &rug::Complex, p1: &rug::Complex) -> LineHitResult {
	if p0.clone().norm().real() < &1e-6 || p1.clone().norm().real() < &1e-6 {
		return LineHitResult::DefinitelyIn
	}
	if signum(p0.imag()) * signum(p1.imag()) >= 0 {
		return LineHitResult::Sign(1);
	}

	let delta = p1.clone() - p0;
	let (dx, dy) = delta.into_real_imag();

	LineHitResult::Sign(signum(&(dx * p0.imag() - dy * p0.real())) * signum(p0.imag()))
}

struct PeriodFinder<'a> {
	points_0: [rug::Complex; 4],
	points:   [rug::Complex; 4],
	period: u32,
	max_period: u32,
	stop_channel: &'a Receiver<()>
}

impl<'a> PeriodFinder<'a> {
	fn new(bounds: (&rug::Complex, &rug::Complex), max_period: u32, stop_channel: &'a Receiver<()>) -> Self {
		let prec = bounds.0.prec();

		let p0 = bounds.0.clone();
		let p2 = bounds.1.clone();
		let p1 = rug::Complex::with_val(prec, (p2.real(), p0.imag()));
		let p3 = rug::Complex::with_val(prec, (p0.real(), p2.imag()));

		let points_0 = [p0, p1, p2, p3];

		Self {
			points: [
				rug::Complex::with_val(prec, 0f64),
				rug::Complex::with_val(prec, 0f64),
				rug::Complex::with_val(prec, 0f64),
				rug::Complex::with_val(prec, 0f64),
			],
			points_0,
			period: 0,
			max_period,
			stop_channel
		}
	}
}

impl<'a> Iterator for PeriodFinder<'a> {
	type Item = u32;

	fn next(&mut self) -> Option<u32> {
		while self.period <= self.max_period {
			if !self.stop_channel.is_empty() {
				let _ = self.stop_channel.recv();
				return None;
			}

			for (p, p0) in self.points.iter_mut().zip(self.points_0.iter_mut()) {
				p.square_mut();
				*p += &*p0;

				let norm = p.clone().norm();
				let (norm, _) = norm.into_real_imag();
				if norm > 256.0 * 256.0 {
					let abs = norm.sqrt();
					*p /= abs;

					*p0 = rug::Complex::with_val(p0.prec(), 0f64);
				}
			}

			self.period += 1;

			let mut prev = self.points.last().unwrap();
			let mut sign = 1;
			for cur in self.points.iter() {
				let result = line_hits_positive_real(prev, cur);
				match result {
					LineHitResult::DefinitelyIn => {
						return Some(self.period);
					}
					LineHitResult::Sign(s) => { sign *= s; }
				}
				prev = cur;
			}
			if sign < 0 {
				return Some(self.period);
			}
		}
		None
	}
}

pub(super) enum ReferenceErrReason {
	NoneFound,
	Stopped
}

fn find_periodic_point(
	initial: &rug::Complex, period: u32, steps: u32, stop_channel: &Receiver<()>
) -> Result<rug::Complex, ReferenceErrReason> {
	let prec = initial.prec();
	let mut c = initial.clone();
	for _ in 0..steps {
		if !stop_channel.is_empty() {
			let _ = stop_channel.recv();
			return Err(ReferenceErrReason::Stopped);
		}

		let mut g = DualComplex::constant(rug::Complex::with_val(prec, (1.0, 0.0)));
		let mut z = DualComplex::constant(rug::Complex::with_val(prec, (0.0, 0.0)));
		let c_dual = DualComplex::variable(c.clone());
		for l in 1..=period {
			z = &z * &z + &c_dual;
			if l < period && period % l == 0 {
				g = &z * &g;
			}
		}
		g = &z / &g;
		c -= g.val / g.diff;
	}
	if c.real().is_nan() || c.imag().is_nan() || c.clone().norm().real() > &256.0 {
		Err(ReferenceErrReason::Stopped)
	} else {
		Ok(c)
	}
}

pub(super) fn find_reference_point_in_view(
	bounds: (&rug::Complex, &rug::Complex), centre: &rug::Complex,
	max_period: u32, steps: u32, stop_channel: &Receiver<()>
) -> Result<rug::Complex, ReferenceErrReason> {
	let finder = PeriodFinder::new(bounds, max_period, stop_channel);

	let mut outside_count = 0;

	let mut best = rug::Complex::with_val(bounds.0.prec(), std::f64::INFINITY);
	for period in finder {
		let point =
			match find_periodic_point(centre, period, steps, stop_channel) {
				Ok(p) => p,
				Err(r) => match r {
					ReferenceErrReason::NoneFound => continue,
					ReferenceErrReason::Stopped   => return Err(r)
				}
			};

		if point.real() >= bounds.0.real() && point.real() <= bounds.1.real() &&
		   point.imag() <= bounds.0.imag() && point.imag() >= bounds.1.imag()    {
			return Ok(point);
		} else if (point.clone() - centre).norm().real() < (best.clone() - centre).norm().real() {
			best = point;
			outside_count += 1;

			if outside_count > 32 { break; }
		}
	}

	if best == std::f64::INFINITY {
		Err(ReferenceErrReason::NoneFound)
	} else {
		Ok(best)
	}
}

// this remaps one range of numbers onto another
pub(super) fn remap_range(x: f64, start_range: (f64, f64), end_range: (&rug::Float, &rug::Float)) -> rug::Float {
	let norm = x / (start_range.1 - start_range.0);
	let new_scale = norm * rug::Float::with_val(BITS, end_range.1 - end_range.0);
	new_scale + end_range.0
}

struct ComplexSimd<S: Simd> where S::Vf64: Copy {
	reals: S::Vf64,
	imags: S::Vf64
}

impl<S: Simd> std::fmt::Debug for ComplexSimd<S> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_tuple("")
			.field(&self.reals)
			.field(&self.imags)
			.finish()
	}
}

impl<S: Simd> Copy for ComplexSimd<S> {}
impl<S: Simd> Clone for ComplexSimd<S> {
	fn clone(&self) -> Self {
		*self
	}
}

impl<S: Simd> ComplexSimd<S> {
	fn from_complex(c: Complex<f64>) -> Self {
		Self {
			reals: unsafe { S::set1_pd(c.re) },
			imags: unsafe { S::set1_pd(c.im) }
		}
	}

	fn norm_sqr(&self) -> S::Vf64 {
		self.reals * self.reals + self.imags * self.imags
	}
}

impl<S: Simd> Add for ComplexSimd<S> {
	type Output = ComplexSimd<S>;

	fn add(self, rhs: Self) -> Self::Output {
		Self::Output {
			reals: self.reals + rhs.reals,
			imags: self.imags + rhs.imags
		}
	}
}

impl<S: Simd> AddAssign for ComplexSimd<S> {
	fn add_assign(&mut self, rhs: Self) {
		self.reals += rhs.reals;
		self.imags += rhs.imags;
	}
}

impl<S: Simd> Mul for ComplexSimd<S> {
	type Output = ComplexSimd<S>;

	fn mul(self, rhs: Self) -> Self::Output {
		Self::Output {
			reals: self.reals * rhs.reals - self.imags * rhs.imags,
			imags: self.reals * rhs.imags + self.imags * rhs.reals
		}
	}
}

impl<S: Simd> MulAssign for ComplexSimd<S> {
	fn mul_assign(&mut self, rhs: Self) {
		*self = *self * rhs;
	}
}

pub(super) fn compute_chunk(
	pixels: &mut Array2<RGB8>, size: (usize, usize), bounds: (&rug::Complex, &rug::Complex),
	reference: &rug::Complex, reference_vec: &[Complex<f64>], glitch_vec: &[f64],
	gradient: &Gradient<LinSrgb<f64>>, stop_channel: &Receiver<()>
) -> Option<()> {
	// zoomed width and height in the complex plane
	let zoomed_width  = rug::Float::with_val(BITS, bounds.1.real() - bounds.0.real()).to_f64();
	let zoomed_height = rug::Float::with_val(BITS, bounds.1.imag() - bounds.0.imag()).to_f64();
	
	// reference delta is the difference between the reference point and the top-left bound corner
	let reference_delta = rug::Complex::with_val(BITS, reference - bounds.0);
	let reference_delta = reference_delta.into_real_imag();
	let reference_delta = Complex::new(reference_delta.0.to_f64(), reference_delta.1.to_f64());

	let glitch_count = AtomicUsize::new(0);

	let res = pixels.slice_mut(s![..size.1, ..size.0]).outer_iter_mut().into_par_iter().enumerate().try_for_each(|(y, mut row)| {
		compute_chunk_simd_internal_runtime_select(
			&mut row, y, size, zoomed_width, zoomed_height, reference_delta, reference_vec, glitch_vec,
			gradient, stop_channel, &glitch_count
		)
	});

	if let Some(_) = res {
		let ratio = glitch_count.into_inner() as f64 / ((size.0 * size.1) as f64);
		if ratio > 0.01 { println!("High glitch ratio: {}", ratio); }
	}

	res
}

simd_runtime_generate!(
	fn compute_chunk_simd_internal(
		row: &mut ndarray::ArrayViewMut1<RGB8>, y: usize, size: (usize, usize), zoomed_width: f64, zoomed_height: f64,
		reference_delta: Complex<f64>, reference_vec: &[Complex<f64>], glitch_vec: &[f64],
		gradient: &Gradient<LinSrgb<f64>>, stop_channel: &Receiver<()>,
		glitch_count: &AtomicUsize
	) -> Option<()> {

		// check the stop channel once per row and if there's a message to stop, stop now
		if !stop_channel.is_empty() {
			let _ = stop_channel.recv();
			return None;
		}

		for x in (0..row.len()).step_by(S::VF64_WIDTH) {
			// this computes the delta relative to the reference without any arbitrary precision
			let mut delta_0_xs = S::set1_pd(0.0);
			for (i, f) in (0..S::VF64_WIDTH).map(|i| x as f64 + i as f64).enumerate() {
				delta_0_xs[i] = f;
			}
			delta_0_xs *= S::set1_pd(zoomed_width);
			delta_0_xs /= S::set1_pd(size.0 as f64);
			delta_0_xs -= S::set1_pd(reference_delta.re);
			let delta_0_ys = S::set1_pd((y as f64 * zoomed_height) / size.1 as f64 - reference_delta.im);

			let delta_0 = ComplexSimd::<S> {
				reals: delta_0_xs,
				imags: delta_0_ys
			};

			let mut delta = delta_0;

			// these are used as masks to make sure that if the entire vector still has things
			// to compute, the stuff we already computed isn't going to change
			// they're also used as boolean arrays via elem != 0
			let mut limit_cmp = S::set1_epi64(0);
			let mut glitch_cmp = S::set1_epi64(0);

			// this tracks the glitch values for a given pixel, they're largely in sync until a glitch happens
			let mut glitch_cmp_value = S::set1_pd(0.0);

			// these track the last useful value we had for these (they're used for colour calculation later)
			let mut last_norm_sqrs = S::set1_pd(0.0);
			let mut last_counts = S::set1_epi64(0);

			for (i, (r, g)) in reference_vec.iter().zip(glitch_vec).enumerate() {
				let norm_sqr = (ComplexSimd::from_complex(*r) + delta).norm_sqr();

				// if a pixel became unlimited or it glitched, stop storing the calculations
				let move_mask = S::or_epi64(limit_cmp, glitch_cmp);

				// essentially, for each element, if an escape/glitch has been detected, use the existing value
				// otherwise use what we just computed
				last_norm_sqrs   = S::blendv_pd   (norm_sqr,                last_norm_sqrs,   S::castepi64_pd(move_mask));
				last_counts      = S::blendv_epi64(S::set1_epi64(i as i64), last_counts,      move_mask);
				glitch_cmp_value = S::blendv_pd   (S::set1_pd(*g),          glitch_cmp_value, S::castepi64_pd(move_mask));

				limit_cmp  = S::castpd_epi64(S::cmpgt_pd(last_norm_sqrs, S::set1_pd(256.0)));
				glitch_cmp = S::castpd_epi64(S::cmplt_pd(last_norm_sqrs, glitch_cmp_value));

				if (0..S::VF64_WIDTH).all(|i| limit_cmp[i] != 0 || glitch_cmp[i] != 0) {
					break;
				}

				// this is equivalent to delta = 2 * delta * r + delta ^ 2 + delta_0
				// i.e. the arbitrary precision delta iteration formula
				delta *= ComplexSimd::from_complex(r + r) + delta;
				delta += delta_0;

			}
			for i in 0..S::VF64_WIDTH {
				let res =
					if glitch_cmp[i] != 0 {
						MandelResult::Glitched
					} else if limit_cmp[i] != 0 {
						let f = last_counts[i] as f64 - last_norm_sqrs[i].log2().log2();
						MandelResult::Unbounded(f)
					} else { MandelResult::Bounded };

				if x + i < row.len() {
					row[x + i] = match res {
						MandelResult::Glitched => {
							glitch_count.fetch_add(1, Ordering::SeqCst);
							RGB8::new(255, 0, 0)
						},
						MandelResult::Bounded => RGB8::new(0u8, 0u8, 0u8),
						MandelResult::Unbounded(c) => {
							let (r, g, b) = gradient.get((c * 0.01) % 1.0).into_components();
							RGB8::new((r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8)
						}
					}
				}
			}
		}
		Some(())
	}
);
